---
author: bgamari
title: "GHC 8.8.1-alpha2 released"
date: 2019-06-19
tags: release
---
The GHC team is pleased to announce the second and likely last alpha
release of GHC 8.8.1. The source distribution, binary distributions, and
documentation are available from
[downloads.haskell.org](https://downloads.haskell.org/~ghc/8.8.1-alpha2).
A draft of the release notes is also [available][1].

This release is the culmination of over 3000 commits by over one hundred
contributors and has several new features and numerous bug fixes
relative to GHC 8.6:

 * Profiling now works correctly on 64-bit Windows (although still may
   be problematic on 32-bit Windows due to platform limitations; see
   #15934)

 * A new code layout algorithm for amd64's native code generator

 * The introduction of a late lambda-lifting pass which may reduce
   allocations significantly for some programs.

 * Further work on Trees That Grow, enabling improved code re-use of the
   Haskell AST in tooling

 * More locations where users can write `forall` ([GHC Proposal #0007][proposal7])

 * Further work on the Hadrian build system

This release brings a number of fixes since alpha 1:

 * A number of linker fixes (#16779, #16784)

 * The process, binary, Cabal, time, terminfo libraries have all been
   bumped to their final release versions

 * A regression rendering TemplateHaskell unusable in cross-compiled
   configurations has been fixed (#16331)

 * A regression causing compiler panics on compilation of some programs
   has been fixed (#16449)

 * `-Wmissing-home-modules` now handles hs-boot files correctly (#16551)

 * A regression causing some programs to fail at runtime has been fixed
   (#16066)

Due to on-going work on our release and testing infrastructure this
cycle is proceeding at a pace significantly slower than expected.
However, we anticipate that this investment will allow us to release a
more reliable, easier-to-install compiler on the planned six-month
release cadence in the future.

As always, if anything looks amiss do let us know.

Happy compiling!

[1]: https://downloads.haskell.org/ghc/8.8.1-alpha2/docs/html/users_guide/8.8.1-notes.html
[proposal7]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0007-instance-foralls.rst

