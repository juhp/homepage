---
author: bgamari
title: "GHC 8.6.5 released"
date: 2019-04-23
tags: release
---

The GHC team is proud to announce the release of GHC 8.6.5. The source
distribution, binary distributions, and documentation are available at
[downloads.haskell.org](https://downloads.haskell.org/~ghc/8.6.5).
Release notes are also [available][relnotes]

This release fixes a handful of issues with 8.6.4:

 - Binary distributions once again ship with Haddock documentation including
   syntax highlighted source of core libraries (#16445)

 - A build system issue where use of GCC with `-flto` broke `configure`
   was fixed (#16440)

 - An bug affecting Windows platforms wherein XMM register values could be
   mangled when entering STG has been fixed (#16514)

 - Several Windows packaging issues resulting from the recent CI rework.
   (#16408, #16398, #16516)

Do note that due to linking issues (see #15934) profiling will be rather
unreliable on Windows. This will be fixed in 8.8.1.

As always, if anything looks amiss do let us know.

Happy compiling!


[relnotes]:http://downloads.haskell.org/~ghc/8.6.5/docs/html/users_guide/8.6.5-notes.html
