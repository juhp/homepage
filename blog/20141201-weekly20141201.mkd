---
author: thoughtpolice
title: "GHC Weekly News - 2014/12/01"
date: 2014-12-01
tags: ghc news
---

Hi \*,

It's that time again for some good ol' fashion GHC news, this time just after the holidays. Some of the things happening in the past week include:

  - Partial Type Signatures has been merged into HEAD. Many thanks to Thomas Winant, who worked on this feature for several months!

  - As mentioned last week, GHC 7.10 will no longer ship `haskell98` and `haskell2010`, nor `old-time` or `old-locale`.

  - Neil Mitchell asked a very good question on `ghc-devs`: what's the process for getting your library synced for the GHC 7.10 release? As the maintainer of `filepath` he'd like to know, and Herbert responded quickly: https://www.haskell.org/pipermail/ghc-devs/2014-November/007394.html

  - Yuras Shumovich has more questions about exception handling, primarily: why is `mask` used in `waitQSem`? Simon Marlow responds: https://www.haskell.org/pipermail/ghc-devs/2014-November/007394.html

  - Carter Schonwald reports that GHC is having problems building on OS X with XCode 6, but Kazu Yamamoto replied he had no problem - perhaps some OS X wizards can help figure out the discrepancy: https://www.haskell.org/pipermail/ghc-devs/2014-November/007394.html

  - Austin Seipp announced GHC 7.8.4 Release Candidate 1, with about a dozen bugfixes for major features: https://www.haskell.org/pipermail/ghc-devs/2014-November/007438.html

  - Gergo Erdi asked about backporting pattern synonym type signatures to 7.8.4. The conclusion? Probably won't happen - it's a bit too late! https://www.haskell.org/pipermail/ghc-devs/2014-November/007440.html

  - Carter Schonwald had some questions about the let-app invariant and primop code, which he was puzzling about to help with some code generation work. There are also some nice discussions of caches and prefetching in pure programs later on, too: https://www.haskell.org/pipermail/ghc-devs/2014-November/007440.html & https://www.haskell.org/pipermail/ghc-devs/2014-November/007410.html 

  - Ben Gamari talked about the current status of GHC and LLVM, GHC on ARM, and the future of our LLVM support on both the mailing lists, and his blog. A good read overall for those interested in the war-stories of GHC-on-ARM: https://www.haskell.org/pipermail/ghc-devs/2014-November/007469.html

Closed tickets this week include: #9827, #7475, #9826, #7460, #7643, #8044, #8031, #7072, #3654, #7033, #9834, #6098, #6022, #5859, #5763, #9838, #9830, #7243, #9736, #9574, #5158, #9844, #9281, #9818, #4429, #8815, #2182, #4290, #9005, #9828, #9833, #9582, and #9850.

Another huge thanks to **Thomas Miedema** who closed an extraordinary amount of tickets for us - the above list is still not even complete, and he's made a huge impact on the amount of open tickets in the past month or so.
